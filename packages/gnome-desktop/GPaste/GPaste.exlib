# Copyright 2012-2014 Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
# Distributed under the terms of the GNU General Public License v2

myexparam gnome_shell_version

exparam -v GS_VER gnome_shell_version

require bash-completion github [ user=Keruspe ] zsh-completion
require freedesktop-desktop gsettings
require vala [ vala_dep=true with_opt=true ]

export_exlib_phases src_install pkg_postinst pkg_postrm

SUMMARY="Clipboard management system"
HOMEPAGE="https://www.github.com/Keruspe/${PN}/"

LICENCES="BSD-2"
SLOT="0"
MYOPTIONS="
    bash-completion
    gnome-shell [[ description = [ Install the gnome-shell extension ] requires = gobject-introspection ]]
    gobject-introspection
    vapi [[ requires = gobject-introspection ]]
    zsh-completion
    ( linguas: de es fi fr pt_BR )
"
    #( providers: gtk3 gtk4 ) [[ number-selected = [ exactly-one ] ]]

DEFAULT_SRC_CONFIGURE_PARAMS=(
    "--enable-x-keybinder"

    "--disable-schemas-compile"
)

#DEFAULT_SRC_CONFIGURE_OPTIONS=(
#    'providers:gtk3 --with-gtk=3.0'
#    'providers:gtk4 --with-gtk=4.0'
#)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    "bash-completion"
    "gnome-shell gnome-shell-extension"
    "gobject-introspection introspection"
    "vapi vala"
    "zsh-completion"
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    "bash-completion bashcompletiondir ${BASHCOMPLETIONDIR}"
    "zsh-completion zshcompletiondir ${ZSHCOMPLETIONDIR}"
)

DEPENDENCIES="
    build:
        dev-libs/appstream-glib
        sys-devel/gettext
        virtual/pkg-config[>=0.27]
    build+run:
        dev-libs/glib:2[>=2.58.0]
        sys-apps/dbus
        x11-libs/gdk-pixbuf:2.0[>=2.38.0]
        x11-libs/gtk+:3[>=3.24.0][gobject-introspection?]
        x11-libs/libX11
        x11-libs/libXi
        gnome-shell? (
            gnome-bindings/gjs:1[>=1.54.0]
            gnome-desktop/mutter
            x11-libs/pango
        )
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.58.0] )
    run:
        gnome-shell? ( gnome-desktop/gnome-shell[${GS_VER}] )
        zsh-completion? ( app-shells/zsh )
"
        #providers:gtk3? ( x11-libs/gtk+:3[>=3.24.0][gobject-introspection?] )
        #providers:gtk4? ( x11-libs/gtk+:4.0[>=3.94.0] )

AT_M4DIR=(m4)

GPaste_src_install() {
    default

    option gobject-introspection || edo rmdir "${IMAGE}"/usr/share/gir-1.0
    option vapi || edo rmdir "${IMAGE}"/usr/share/vala{/vapi,}
}

GPaste_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gsettings_pkg_postinst
}

GPaste_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gsettings_pkg_postrm
}
